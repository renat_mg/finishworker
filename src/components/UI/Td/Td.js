import React from 'react'
import classes from './Td.module.scss'

export default props => {
    let fontWeight = props.fontWeight || 'normal'
    let [textAlign, justifyContent] = props.align ? [props.align, props.align] : ['left', 'left']
    let minWidth = props.width || null
    let background = props.bg || null
    let onClickHandler = props.onClickHandler || null
    let padding = props.sub ? "0 2rem" : null


    return (

        <td className={classes.Td} colSpan={props.colSpan} style={{minWidth, background}}
            onClick={onClickHandler}>
            <div className={classes.TdItem} style={{fontWeight, justifyContent, padding}}>
                <div style={{textAlign}}>
                    {
                        props.children
                    }
                </div>
            </div>
        </td>
    )
}