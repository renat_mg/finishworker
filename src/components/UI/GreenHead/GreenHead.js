import React, {useState} from 'react'
import classes from './GreenHead.module.scss'

export default props => {
    let isOpen = false
    let cursor = 'default'

    if (props.eventKey) {
        isOpen = +props.eventKey === +props.active.id && props.active.isOpen
        cursor = 'pointer'
    }


    return (
        <div className={classes.GreenHead} onClick={props.onClick} style={{cursor}}>
            <div>
                {props.children}
            </div>
            {
                props.eventKey
                    ?

                    <div>
                        {
                            isOpen
                                ? <svg width="12" height="7" viewBox="0 0 12 7" fill="none"
                                       xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" clipRule="evenodd"
                                          d="M0.122781 6.53768C0.288173 6.70944 0.556935 6.70944 0.722328 6.53768L5.9942 1.0519L11.2764 6.53768C11.4418 6.70945 11.7106 6.70945 11.876 6.53768C12.0413 6.36591 12.0413 6.08679 11.876 5.91503L6.30431 0.128661C6.22162 0.0427785 6.11825 -0.00016259 6.00454 -0.000162599C5.90117 -0.000162608 5.78746 0.0427785 5.70477 0.128661L0.133118 5.91503C-0.0426119 6.08679 -0.0426111 6.36591 0.122781 6.53768Z"
                                          fill="#292929"/>
                                </svg>
                                : <svg width="12" height="7" viewBox="0 0 12 7" fill="none"
                                       xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" clipRule="evenodd"
                                          d="M11.8772 0.128824C11.7118 -0.0429415 11.4431 -0.0429415 11.2777 0.128824L6.0058 5.6146L0.72359 0.128824C0.558198 -0.0429415 0.289436 -0.0429415 0.124044 0.128824C-0.0413481 0.30059 -0.0413481 0.57971 0.124044 0.751476L5.69569 6.53784C5.77838 6.62372 5.88176 6.66667 5.99546 6.66667C6.09883 6.66667 6.21254 6.62372 6.29524 6.53784L11.8669 0.751476C12.0426 0.57971 12.0426 0.30059 11.8772 0.128824Z"
                                          fill="#292929"/>
                                </svg>
                        }
                    </div>
                    : null
            }
        </div>
    )
}