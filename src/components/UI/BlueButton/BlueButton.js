import React from 'react'
import classes from './BlueButton.module.scss'
import {NavLink} from 'react-router-dom'

export default props => {
    const height = props.height || 60
    const flexBasis = props.flex || 200
    return (
        <>
            {props.navLink ?
                <NavLink
                    to={props.to}
                    className={classes.BlueButton}
                    style={{height, flexBasis}}
                    >
                    {props.children}
                </NavLink>
                : <div
                    role={"button"}
                    className={classes.BlueButton}
                    style={{height, flexBasis}}
                    onClick={props.onClick}>
                    {props.children}
                </div>
            }

        </>
    )
}

