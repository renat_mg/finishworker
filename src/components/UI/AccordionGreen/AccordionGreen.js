import React from 'react'
import classes from './AccordionGreen.module.scss'

import GreenHead from '../GreenHead/GreenHead'


export default props => {
    return (
        <>
            <GreenHead/>
            {props.children}
        </>
    )
}