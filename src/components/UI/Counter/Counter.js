import React, {useState} from 'react'
import classes from './Counter.module.scss'

export default props => {
    // console.log(props)
    // let inputValue = props.table.amount
    const [count, setCount] = useState(props.table.amount);

    const inputHandler = (event) => {
        if (count !== event.target.value.trim()) {
            setCount(event.target.value.trim())
            props.tableHandler(props.table.id, event.target.value.trim(), 'amount')
        }
    }
    const minusBtn = () => {
        if (+count - 1 >= 0) {
            setCount(+count - 1)
            props.tableHandler(props.table.id, +count - 1, 'amount')
        }
    }
    const plusBtn = () => {
        setCount(+count + 1)
        props.tableHandler(props.table.id, +count + 1, 'amount')
    }

    return (
        <React.Fragment>
            <div className={props.counterBtn} onClick={minusBtn}>-</div>
            <div className={props.counterInput}>
                <input
                    onChange={event => inputHandler(event)}
                    type="text"
                    value={count}
                />
            </div>
            <div className={props.counterBtn} onClick={plusBtn}>+</div>
        </React.Fragment>
    )
}