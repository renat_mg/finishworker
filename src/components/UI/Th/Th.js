import React from 'react'
import classes from './Th.module.scss'

export default props => {

    let fontWeight = props.fontWeight ? props.fontWeight : 'normal'
    let [textAlign, justifyContent] = props.align ? [props.align, props.align] : ['left', 'left']

    return (

        <th className={classes.Th} colSpan={props.colSpan}>
            <div className={classes.ThItem} style={{fontWeight, justifyContent}}>
                <div style={{textAlign}}>
                    {
                        props.children
                    }
                </div>
            </div>
        </th>
    )
}