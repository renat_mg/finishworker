import React, {useState} from 'react'
import classes from './ServiceCalcBottom.module.scss'

import Th from '../UI/Th/Th'
import Td from '../UI/Td/Td'
import TooltipBlueWhite from '../UI/TooltipBlueWhite/TooltipBlueWhite'
import GreenHead from '../UI/GreenHead/GreenHead'

import Accordion from 'react-bootstrap/Accordion'
import {useAccordionToggle} from 'react-bootstrap/AccordionToggle';
import Table from 'react-bootstrap/Table'
import * as moment from 'moment'
import 'moment/locale/ru'




export default props => {

    const [active, setActive] = useState(
        {
            id: "1",
            isOpen: true
        }
    )

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }


    function CustomToggle({children, eventKey}) {

        const decoratedOnClick = useAccordionToggle(eventKey, evt => {
            let isOpen = +eventKey === +active.id ? !active.isOpen : true
            setActive({
                id: eventKey,
                isOpen
            })
        });


        return (
            <GreenHead
                onClick={decoratedOnClick}
                active={active}
                eventKey={eventKey}
            >
                {children}
            </GreenHead>
        );
    }

    function createDateRow(table) {
        let row = [];

        for (let i = 0; i < +table.days; i++) {
            row.push(<Th key={i}>{moment(table.dateStart).add(i, 'd').format("dddd").capitalize()},<br/> {i+1} день </Th>)
        }
        return row
    }

    function createAmountRow(table) {
        let row = [];
        let sum = props.getSum(table.id)
        for (let i = 0; i < +table.days; i++) {
            row.push(<Td key={i} width={120}>{(table.amount / table.days).toFixed(1)}</Td>)
        }
        return row
    }

    function createFinanceRow(table) {
        let row = [];

        let sum = props.getSum(table.id)
        for (let i = 0; i < +table.days; i++) {
            row.push(<Td key={i} width={120}>{(sum / table.days).toFixed()}</Td>)
        }
        return row
    }


    return (
        <>
            <div className={classes.ServiceCalcBottomTitle}>Количество мастеров для отдельных видов работ:</div>


            <Accordion defaultActiveKey="1" bsPrefix="greenHeadAcc">

                {props.tables.map((table, idx) => {

                    if (typeof table.id !== "undefined") {

                        let service = props.getService(table.id)
                        let workerCount = props.getWorkerCount(table.id)

                        let clsRightBlock =[classes.RightBlock]
                        if(+table.days === 6 || +table.days === 5){
                            clsRightBlock.push(classes["scroll-6"])
                        }
                        if(+table.days === 3 || +table.days === 4){
                            clsRightBlock.push(classes["scroll-4"])
                        }
                        if(table.days < 3){
                            clsRightBlock.push(classes["scroll-2"])
                        }

                        return (

                            <div
                                key={idx}
                                className={"mb-4"}>

                                <CustomToggle eventKey={idx.toString()}>
                                    <div>Наименование: <strong>{service.name}</strong>, требуемое количество
                                        мастеров: <strong>{workerCount}</strong></div>
                                </CustomToggle>
                                <Accordion.Collapse eventKey={idx.toString()}>
                                    <div className={classes.ServiceCalcBottomContainer}>
                                        <div className={classes.LeftBlock}>
                                            <Table borderless>
                                                <thead>
                                                <tr className={classes.TableHeadRow}>
                                                    <Th colSpan={2}>

                                                    </Th>
                                                    <Th>
                                                        Ед. изм.
                                                    </Th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr className={classes.TableBodyRow}>
                                                    <Td colSpan={2}>Объемы <TooltipBlueWhite right={"35%"} top={5}
                                                                                             content={'Равномерно распределены по дням '}/></Td>
                                                    <Td>м2</Td>
                                                </tr>
                                                <tr className={classes.TableBodyRow}>
                                                    <Td colSpan={2}>Фин. выполнение</Td>
                                                    <Td>руб.</Td>
                                                </tr>

                                                </tbody>

                                            </Table>
                                        </div>
                                        <div className={clsRightBlock.join(" ")}>
                                            <Table borderless>
                                                <thead>
                                                <tr className={classes.TableHeadRow}>
                                                    {createDateRow(table)}
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr className={classes.TableBodyRow}>
                                                    {createAmountRow(table)}
                                                </tr>
                                                <tr className={classes.TableBodyRow}>
                                                    {createFinanceRow(table)}
                                                </tr>
                                                </tbody>
                                            </Table>

                                        </div>
                                    </div>
                                </Accordion.Collapse>
                            </div>
                        )
                    }

                })}
            </Accordion>


        </>
    )
}