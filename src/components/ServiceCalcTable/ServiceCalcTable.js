import React from 'react'
import classes from './ServiceCalcTable.module.scss'
import ServiceCalcTableHead from './ServiceCalcTableHead/ServiceCalcTableHead'
import ServiceCalcRows from './ServiceCalcRows/ServiceCalcRows'
import ServiceCalcTableTotal from './ServiceCalcTableTotal/ServiceCalcTableTotal'
import Table from 'react-bootstrap/Table'

export default props => {

    return (
        <>
            <Table responsive borderless>
                <ServiceCalcTableHead
                    tableHead={props.tableHead}
                />
                <ServiceCalcRows
                    tables={props.tables}
                    services={props.services}
                    workAdditional={props.workAdditional}
                    workType={props.workType}
                    tableHandler={props.tableHandler}
                    deleteTableHandler={props.deleteTableHandler}
                    getService={props.getService}
                    getWorkerCount={props.getWorkerCount}
                />
            </Table>
            <ServiceCalcTableTotal
                tables={props.tables}
                getWorkerCount={props.getWorkerCount}
                getSum={props.getSum}
            />

        </>
    )
}