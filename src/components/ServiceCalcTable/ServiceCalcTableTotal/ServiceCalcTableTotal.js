import React from 'react'
import classes from './ServiceCalcTableTotal.module.scss'
import NumberFormat from 'react-number-format'

export default props => {
    let sum = 0
    let workerCount = 0
    props.tables.forEach(table => {
        let amount = props.getSum(table.id)
        if (amount) {
            sum += amount
        }
        let worker = props.getWorkerCount(table.id)
        if (worker) {
            workerCount += worker
        }
    })
    return (
        <div className={classes.ServiceCalcTableTotal}>
            <div>
                <span>Сумма заказа:&nbsp;</span>
                <NumberFormat value={sum} displayType={'text'} thousandSeparator={" "}/>
                <span> руб.</span>
            </div>
            <div>
                <span>Итого мастеров: </span>
                <span>{workerCount}</span>
            </div>
        </div>
    )
}