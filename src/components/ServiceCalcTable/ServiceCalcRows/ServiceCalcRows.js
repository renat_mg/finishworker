import React from 'react'
import classes from './ServiceCalcRows.module.scss'
import Counter from '../../UI/Counter/Counter'
import Td from '../../UI/Td/Td'
import Th from '../../UI/Th/Th'
import NumberFormat from 'react-number-format'
import * as moment from 'moment'


const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

export default props => {

    let renderRow = (table, idx) => {

        let service = props.getService(table.id)

        if (!table.days) {
            table.days = 1
        }
        let workerCount = props.getWorkerCount(table.id)
        return (
            <tr key={table.id} data-id={table.id} className={classes.ServiceCalcRow}>
                <Th fontWeight={"bold"}>{idx}</Th>
                <Td colSpan={3} align={"left"} width={220}>{service.name}</Td>
                <Td width={50}>{entities.decode(service.unit)}</Td>
                <Td>
                    <div className={classes.rowCounter}>
                        <Counter
                            counterBtn={classes.counterBtn}
                            counterInput={classes.counterInput}
                            table={table}
                            tableHandler={props.tableHandler}
                        />
                    </div>
                </Td>
                <Td width={90} align={"center"}>
                    <NumberFormat
                        value={service.price}
                        displayType={'text'}
                        thousandSeparator={" "}/>
                </Td>
                <Td width={120} align={"center"}>
                    <NumberFormat
                        value={service.price * table.amount}
                        displayType={'text'}
                        thousandSeparator={" "}/>
                </Td>
                <Td>{moment(table.dateStart).format("DD.MM.YYYY")} </Td>
                <Td bg={"#fff"}>{moment(table.dateStart).add((+table.days - 1), 'd').format("DD.MM.YYYY")} </Td>
                <Td align={"center"}>
                    <input
                        disabled
                        className={classes.WorkerCount}
                        value={workerCount}
                    /></Td>
                <Td onClickHandler={props.deleteTableHandler} align={"center"}>
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M15 0C6.72923 0 0 6.72865 0 15C0 23.2713 6.72923 30 15 30C23.2708 30 30 23.2713 30 15C30 6.72865 23.2708 0 15 0ZM15 28.8462C7.36558 28.8462 1.15385 22.6344 1.15385 15C1.15385 7.36558 7.36558 1.15385 15 1.15385C22.6344 1.15385 28.8462 7.36558 28.8462 15C28.8462 22.6344 22.6344 28.8462 15 28.8462Z"
                            fill="#D0D0D0"/>
                        <path
                            d="M20.6 9.39965C20.3745 9.17407 20.0098 9.17407 19.7843 9.39965L14.9998 14.1841L10.2154 9.39965C9.98984 9.17407 9.62523 9.17407 9.39965 9.39965C9.17407 9.62523 9.17407 9.98984 9.39965 10.2154L14.1841 14.9998L9.39965 19.7843C9.17407 20.0098 9.17407 20.3745 9.39965 20.6C9.51215 20.7125 9.65984 20.7691 9.80754 20.7691C9.95523 20.7691 10.1029 20.7125 10.2154 20.6L14.9998 15.8156L19.7843 20.6C19.8968 20.7125 20.0445 20.7691 20.1921 20.7691C20.3398 20.7691 20.4875 20.7125 20.6 20.6C20.8256 20.3745 20.8256 20.0098 20.6 19.7843L15.8156 14.9998L20.6 10.2154C20.8256 9.98984 20.8256 9.62523 20.6 9.39965Z"
                            fill="#D0D0D0"/>
                    </svg>
                </Td>
            </tr>
        )
    }


    return (
        <tbody className={classes.ServiceCalcRows}>
        {props.tables.map((table, idx) => {
            if (Object.keys(table).length !== 0) {
                return renderRow(table, idx)
            }
        })}
        </tbody>
    )
}
