import React from 'react'
import './ServiceTable.scss'
// import classes from './ServicesTable.module.scss'
import FirstRow from './FirstRow/FirstRow'
import SecondRow from './SecondRow/SecondRow'
import ThirdRow from './ThirdRow/ThirdRow'
import FourthRow from './FourthRow/FourthRow'

export default props => {

    // let ServiceTableWrapper = [classes.ServiceTableWrapper]
    // ServiceTableWrapper.push("mt-2 mt-sm-4 pt-3")

    return (
        <div className={"ServiceTableWrapper"}>
            <div className={"ServiceTableHeading"} data-id={props.table.id}>#{props.rowNum}   {props.table.id > 1 ? <button className={"ServiceTableDeleteBtn"} onClick={props.deleteTableHandler}> Удалить работу</button> : ''}</div>

            <FirstRow
                table={props.table}
                serviceTableInputHandler={props.serviceTableInputHandler}
                services={props.services}
            />

            <hr className="d-block d-sm-none"/>

            <SecondRow
                table={props.table}
                serviceTableInputHandler={props.serviceTableInputHandler}
                services={props.services}
            />

            <hr className="d-block d-sm-none"/>

            <ThirdRow
                table={props.table}
                serviceTableBtnHandler={props.serviceTableBtnHandler}
                btns={props.workAdditional}
            />

            <FourthRow
                table={props.table}
                serviceTableBtnHandler={props.serviceTableBtnHandler}
                btns={props.workType}
            />

        </div>
    )
}