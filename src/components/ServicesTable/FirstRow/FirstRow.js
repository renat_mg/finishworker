import React from 'react'
import classes from './FirstRow.module.scss'

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

export default props => {

    let ServiceTableLine = [classes.ServiceTableLine]
    ServiceTableLine.push("ServiceTableLine")

    let ServiceTableCellTitle = [classes.ServiceTableCellTitle]
    ServiceTableCellTitle.push("ServiceTableCellTitle")

    let ServiceTableCellDesc = [classes.ServiceTableCellDesc]
    ServiceTableCellDesc.push("ServiceTableCellDesc")

    let ServiceTableCellInput = [classes.ServiceTableCellInput]
    ServiceTableCellInput.push("ServiceTableCellInput")

    let ServiceTableCellList = [classes.ServiceTableCellList]
    ServiceTableCellList.push("ServiceTableCellList")

    let ServiceTableCellListDecor = [classes.ServiceTableCellListDecor]
    ServiceTableCellListDecor.push("ServiceTableCellListDecor")

    return (
        <div
            className={ServiceTableLine.join(" ")}>

            <div className={ServiceTableCellTitle.join(" ")}>Вид работы
            </div>

            <div className={ServiceTableCellList.join(" ")}>
                <select
                    className="form-control px-3"
                    onChange={evt => props.serviceTableInputHandler(props.table.id, evt, 'serviceId')}
                    value={props.table.serviceId}
                >
                    {props.services.map(service => {
                        return (
                            <option
                                key={service.id}
                                value={service.id}>
                                {service.name}
                            </option>
                        )
                    })}
                </select>
            </div>

            <div className={ServiceTableCellDesc.join(" ")}>
                Объем выполнения
                {props.services.map(service => {
                    return (
                        <span key={service.id}>
                                {+props.table.serviceId === +service.id ? ', ' + entities.decode(service.unit) : ''}
                            </span>
                    )
                })}
            </div>


            <div className={ServiceTableCellInput.join(" ")}>
                <input
                    type="number"
                    pattern="[0-9]*"
                    inputMode="numeric"
                    placeholder="Введите объем"
                    className="form-control px-3"
                    onChange={evt => props.serviceTableInputHandler(props.table.id, evt, 'amount')}
                    value={props.table.amount}
                />
            </div>

            <div className={ServiceTableCellListDecor.join(" ")}></div>


        </div>
    )
}