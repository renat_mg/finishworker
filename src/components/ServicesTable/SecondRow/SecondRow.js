import React from 'react'
import classes from './SecondRow.module.scss'

export default props => {

    let ServiceTableLine = [classes.ServiceTableLine]
    ServiceTableLine.push("ServiceTableLine")

    let ServiceTableCellTitle = [classes.ServiceTableCellTitle]
    ServiceTableCellTitle.push("ServiceTableCellTitle")

    let ServiceTableCellDesc = [classes.ServiceTableCellDesc]
    ServiceTableCellDesc.push("ServiceTableCellDesc")

    let ServiceTableCellInput = [classes.ServiceTableCellInput]
    ServiceTableCellInput.push("ServiceTableCellInput")

    let ServiceTableCellList = [classes.ServiceTableCellList]
    ServiceTableCellList.push("ServiceTableCellList")

    let ServiceTableCellListDecor = [classes.ServiceTableCellListDecor]
    ServiceTableCellListDecor.push("ServiceTableCellListDecor")

    return (
        <div
            className={ServiceTableLine.join(" ")}>
            <div className={ServiceTableCellTitle.join(" ")}>Начало
                работ
            </div>
            <div
                className={ServiceTableCellList.join(" ")}>
                <input
                    type="date"
                    className="form-control px-3 dp__js"
                    placeholder="Дата"
                    onChange={evt => props.serviceTableInputHandler(props.table.id, evt, 'dateStart')}
                    value={props.table.dateStart}
                />
            </div>
            <div className={ServiceTableCellDesc.join(" ")}>Срок
                выполнения (дней)
            </div>
            <div
                className={ServiceTableCellInput.join(" ")}>
                <input
                    type="number"
                    pattern="[0-9]*"
                    inputMode="numeric"
                    placeholder="Введите кол-во"
                    className="form-control px-3"
                    onChange={evt => props.serviceTableInputHandler(props.table.id, evt, 'days')}
                    value={props.table.days}
                />
            </div>
            <div className={ServiceTableCellListDecor.join(" ")}></div>
        </div>
    )
}