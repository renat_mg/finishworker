import React from 'react'
import classes from './ThirdRow.module.scss'
import TooltipWhiteBlue from '../../UI/TooltipWhiteBlue/TooltipWhiteBlue'

export default props => {

    let ServiceTableLine = [classes.ServiceTableLine]
    ServiceTableLine.push("ServiceTableLine")

    let ServiceTableCellTitle = [classes.ServiceTableCellTitle]
    ServiceTableCellTitle.push("ServiceTableCellTitle")

    let ServiceTableCellBtns = [classes.ServiceTableCellBtns]
    ServiceTableCellBtns.push("ServiceTableCellBtns")

    let ServiceTableCellArrow = [classes.ServiceTableCellArrow]
    ServiceTableCellArrow.push("ServiceTableCellArrow")

    let ServiceTableCellListDecor = [classes.ServiceTableCellListDecor]
    ServiceTableCellListDecor.push("ServiceTableCellListDecor")


    return (

        <div
            className={ServiceTableLine.join(" ")}>

            <div className={ServiceTableCellTitle.join(" ")}>
                <span>Выберите УСЛУГУ (Работа / Поставка материалов)</span>
                <span className={ServiceTableCellArrow.join(" ")}></span>
            </div>

            <div
                className={ServiceTableCellBtns.join(" ")}>
                {props.btns.map(btn => {
                    const withTooltip = btn.tooltip.length > 0 ? true : false
                    const isActive = btn.id === props.table.workAdditional ? true : false

                    let ServiceTableCellBtn = [classes.ServiceTableCellBtn]
                    ServiceTableCellBtn.push("ServiceTableCellBtn")
                    if (isActive) {
                        ServiceTableCellBtn.push('active')
                    }

                    return (

                        <div
                            key={btn.id}
                            role="link"
                            data-id={btn.id}
                            className={ServiceTableCellBtn.join(" ")}
                            onClick={evt => props.serviceTableBtnHandler(props.table.id, evt, 'workAdditional')}
                        >
                            {btn.name}
                            {withTooltip
                                ? <TooltipWhiteBlue content={btn.tooltip} />
                                : ''}
                        </div>

                    )
                })}
            </div>

            <div className={ServiceTableCellListDecor.join(" ")}></div>
        </div>


    )
}