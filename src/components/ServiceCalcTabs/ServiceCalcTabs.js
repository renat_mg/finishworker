import React from 'react'
import classes from './ServiceCalcTabs.module.scss'
export default props=>{

    return (
        <div className={classes.ServiceCalcTabs}>
            {props.tabs.map(tab => {
                let ServiceCalcTab = [classes.ServiceCalcTab]
                if (tab.active) {
                    ServiceCalcTab.push(classes.active)
                }
                return (
                    <div
                        key={tab.id}
                        className={ServiceCalcTab.join(" ")}
                        onClick={evt => props.tabsHandler('tabs', evt)}
                        data-id={tab.id}>
                        <strong>{tab.name}</strong>
                    </div>
                )
            })}
        </div>
    )
}