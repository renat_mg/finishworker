import React from 'react'
import classes from './MaterialCalcTable.module.scss'
import MaterialCalcTableHead from './MaterialCalcTableHead/MaterialCalcTableHead'
import MaterialCalcRows from './MaterialCalcRows/MaterialCalcRows'
// import ServiceCalcTableTotal from './ServiceCalcTableTotal/ServiceCalcTableTotal'
import Table from 'react-bootstrap/Table'
import MaterialCalcTable from "../../containers/ServiceCalc/ServiceCalc";

export default props => {

    return (
        <React.Fragment>
            <Table responsive borderless>
                <MaterialCalcTableHead
                    tableHead={props.tableHead}
                />
                <MaterialCalcRows
                    tables={props.tables}
                    services={props.services}
                    tableHandler={props.tableHandler}
                    getService={props.getService}
                    getMaterial={props.getMaterial}
                    deleteMaterialHandler={props.deleteMaterialHandler}
                />
            </Table>
            {/*<ServiceCalcTableTotal*/}
                {/*tables={props.tables}*/}
                {/*getWorkerCount={props.getWorkerCount}*/}
                {/*getSum={props.getSum}*/}
            {/*/>*/}

        </React.Fragment>
    )
}