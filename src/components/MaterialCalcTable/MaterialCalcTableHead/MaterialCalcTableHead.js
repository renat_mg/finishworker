import React from 'react'
import classes from './MaterialCalcTableHead.module.scss'
import TooltipBlueWhite from '../../UI/TooltipBlueWhite/TooltipBlueWhite'
import Th from '../../UI/Th/Th'

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

export default props => {

    return (
        <thead>
        <tr className={classes.TableHead}>
            {props.tableHead.map((item, idx) => {
                let withTooltip = false
                if (item.tooltip) {
                    withTooltip = item.tooltip.length > 0 ? true : false
                }

                let fontWeight = null
                // if (item.id === 9) {
                //     fontWeight = 'bold'
                // }

                let align = null
                // if (item.id === 4 || item.id === 6) {
                //     align = 'center'
                // }

                return (
                    <Th
                        key={idx}
                        fontWeight={fontWeight}
                        align = {align}
                        colSpan={item.col ? item.col : 1}>
                    {entities.decode(item.name)}
                    {withTooltip
                        ? <TooltipBlueWhite content={item.tooltip}/>
                        : ''}

                    </Th>
                )
            })}

        </tr>
        </thead>
    )
}