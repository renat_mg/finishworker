import React from 'react'
import classes from './ServiceCalcTableWorks.module.scss'

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


import TooltipBlueWhite from '../UI/TooltipBlueWhite/TooltipBlueWhite'
import Counter from '../UI/Counter/Counter'
import NumberFormat from 'react-number-format'
import * as moment from 'moment'

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();


export default props => {
    console.log(props.tableHead)
    const head = props.tableHead
    let renderRow = (table, idx) => {
        let service = getService(props.services, table)
        if (!table.days) {
            table.days = 7
        }
        let workerCount = table.amount / (service.norm * table.days)
        if (workerCount < 1) {
            workerCount = 1
        }
        workerCount = Math.round(workerCount)
        return (


            <TableRow key={idx} className={classes.ServiceCalcRow}>
                <TableCell component="th" scope="row">{idx + 1}</TableCell>
                <TableCell align="right" className={classes.rowWork}>{service.name}</TableCell>
                <TableCell align="right" className={classes.CellCentred}>{entities.decode(service.unit)}</TableCell>
                <TableCell align="right" className={classes.rowCounter}>
                    <Counter
                        counterBtn={classes.counterBtn}
                        counterInput={classes.counterInput}
                        table={table}
                        tableHandler={props.tableHandler}
                    />
                </TableCell>
                <TableCell align="right" className={classes.CellCentred}>
                    <NumberFormat
                        value={service.price}
                        displayType={'text'}
                        thousandSeparator={" "}/>
                </TableCell>
                <TableCell align="right" className={classes.CellCentred}>
                    <NumberFormat
                        value={service.price * table.amount}
                        displayType={'text'}
                        thousandSeparator={" "}/>
                </TableCell>
                <TableCell align="right"
                           className={classes.CellCentred}>{moment(table.dateStart).format("DD.MM.YYYY")} </TableCell>
                <TableCell align="right"
                           className={classes.CellCentred}>{moment(table.dateStart).add(+table.days, 'd').format("DD.MM.YYYY")} </TableCell>
                <TableCell align="right" className={classes.CellCentred}>
                    <input
                        disabled
                        className={classes.WorkerCount}
                        value={workerCount}
                        // onChange={props.tableHandler(table.id, workerCount, 'workerCount')}
                        onChange={console.log(workerCount)}
                    /></TableCell>
                <TableCell align="right" className={classes.CellCentred}>
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M15 0C6.72923 0 0 6.72865 0 15C0 23.2713 6.72923 30 15 30C23.2708 30 30 23.2713 30 15C30 6.72865 23.2708 0 15 0ZM15 28.8462C7.36558 28.8462 1.15385 22.6344 1.15385 15C1.15385 7.36558 7.36558 1.15385 15 1.15385C22.6344 1.15385 28.8462 7.36558 28.8462 15C28.8462 22.6344 22.6344 28.8462 15 28.8462Z"
                            fill="#D0D0D0"/>
                        <path
                            d="M20.6 9.39965C20.3745 9.17407 20.0098 9.17407 19.7843 9.39965L14.9998 14.1841L10.2154 9.39965C9.98984 9.17407 9.62523 9.17407 9.39965 9.39965C9.17407 9.62523 9.17407 9.98984 9.39965 10.2154L14.1841 14.9998L9.39965 19.7843C9.17407 20.0098 9.17407 20.3745 9.39965 20.6C9.51215 20.7125 9.65984 20.7691 9.80754 20.7691C9.95523 20.7691 10.1029 20.7125 10.2154 20.6L14.9998 15.8156L19.7843 20.6C19.8968 20.7125 20.0445 20.7691 20.1921 20.7691C20.3398 20.7691 20.4875 20.7125 20.6 20.6C20.8256 20.3745 20.8256 20.0098 20.6 19.7843L15.8156 14.9998L20.6 10.2154C20.8256 9.98984 20.8256 9.62523 20.6 9.39965Z"
                            fill="#D0D0D0"/>
                    </svg>
                </TableCell>
            </TableRow>
        )
    }
    let getService = (services, table) => {
        let service = services.filter(service => {
            return table.id === service.id
        })
        return service[0]
    }


    return (
        <TableContainer component={Paper}>
            <Table className={classes.Table} aria-label="simple table">
                <TableHead className={classes.TableHead}>
                    <TableRow>

                        {head.map((item, idx) => {
                            let withTooltip = false
                            if (item.tooltip) {
                                withTooltip = item.tooltip.length > 0 ? true : false
                            }
                            return (
                                <TableCell
                                    key={idx}
                                    align="right"
                                    className={classes.TableHeadItem}
                                    scope="col"
                                    colSpan={item.col ? item.col : 1}>
                                    {entities.decode(item.name)}
                                    {withTooltip
                                        ? <TooltipBlueWhite content={item.tooltip}/>
                                        : ''}
                                </TableCell>
                            )
                        })}
                        <TableCell scope="col"></TableCell>
                    </TableRow>
                </TableHead>


                <TableBody>
                    {props.tables.map((table, idx) => {
                        return renderRow(table, idx)
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
}