import React from 'react'
import classes from './ServicesTabs.module.scss'

export default props => {
    let ServiceTabs = [classes.ServiceTabs]
    ServiceTabs.push("d-flex flex-column flex-sm-row mt-2 pt-sm-4")

    return (
        <div className={ServiceTabs.join(" ")}>
            {props.tabs.map(tab => {
                let ServiceTab = ["d-flex align-items-center px-2 px-sm-3 mb-3 mb-sm-0 mr-0 mr-sm-3"]
                if (tab.active) {
                    ServiceTab.push(classes.active)
                }
                return (
                    <div
                        key={tab.id}
                        className={ServiceTab.join(" ")}
                        onClick={evt => props.tabsHandler('tabs', evt)}
                        data-id={tab.id}>
                        Заказ &nbsp;
                        <strong>{tab.name}</strong>
                    </div>
                )
            })}
        </div>
    )
}