import React from 'react'
import classes from './ServiceControls.module.scss'
import {NavLink} from 'react-router-dom'

import GreenButton from '../UI/GreenButton/GreenButton'
import BlueButton from '../UI/BlueButton/BlueButton'

export default props => {
    return (
        <div className={classes.ServiceControls}>
            <BlueButton
                onClick={props.serviceAddBtnHandler}>
                Добавить работы
            </BlueButton>
            <GreenButton
                to={"/services/calculation"}
                navLink>
                Показать стоимость
            </GreenButton>
        </div>
    )
}