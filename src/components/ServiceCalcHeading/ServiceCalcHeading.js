import React from 'react'
import classes from './ServiceCalcHeading.module.scss'

import GreenHead from '../UI/GreenHead/GreenHead'

export default props => {

    return (
        <GreenHead>
            <div>Ваш заказ №</div>
            <div className={classes.orderNum}>{props.order.id}</div>
            <div>От <strong>{props.order.date}</strong></div>
        </GreenHead>
    )
}
