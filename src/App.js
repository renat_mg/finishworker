import React from 'react';
// import logo from './logo.svg';
import 'typeface-roboto';
import './App.css';
import './App.scss'

import Layout from './hoc/Layout/Layout'
import Services from './containers/Services/Services'

function App() {

    return (
        <Layout>
            <header></header>
            <Services/>
            <footer></footer>
        </Layout>
    );
}

export default App;
