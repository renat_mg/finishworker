import React from 'react'
import classes from './ServiceSelect.module.scss'

import ServicesTabs from '../../components/ServicesTabs/ServicesTabs'
import ServicesTable from '../../components/ServicesTable/ServicesTable'
import ServiceControls from '../../components/ServiceControls/ServiceControls'

class ServiceSelect extends React.Component {

    state = {
        heading: 'Услуги',
        isServiceSelectPage: true,
        tabs: [
            {
                'id': 1,
                'name': 'отдельных видов работ',
                'active': true
            },
            {
                'id': 2,
                'name': 'комплекса работ “под ключ”',
                'active': false
            },
            {
                'id': 3,
                'name': 'мастера по часовой',
                'active': false
            }
        ],
        tables: [
            {
                'id': 1,
                'serviceId': 1,
                'amount': '',
                'dateStart': '',
                'days': '',
                'workAdditional': 2,
                'workType': 1
            }
        ],
        services: [
            {
                'id': 1,
                'name': 'Плиточные работы / полы',
                'unit': 'м&sup2;',
            },
            {
                'id': 2,
                'name': 'Штукатурные работы',
                'unit': 'м&sup2;',
            },
            {
                'id': 3,
                'name': 'Покрасочные работы',
                'unit': 'м&sup2;',
            },
            {
                'id': 4,
                'name': 'Кровельные работы',
                'unit': 'м&sup2;',
            },
            {
                'id': 5,
                'name': 'Бетонные работы',
                'unit': 'м&sup3;',
            }
        ],
        workAdditional: [
            {
                'id': 1,
                'name': 'Работа',
                'tooltip': ''
            },
            {
                'id': 2,
                'name': 'Работа c поставкой материалов',
                'tooltip': 'Расчет вместе с поставкой строительных и расходных материалов'
            }
        ],
        workType: [
            {
                'id': 1,
                'name': 'Стандарт',
                'tooltip': ''
            },
            {
                'id': 2,
                'name': 'Премиум',
                'tooltip': 'Расчет вместе с поставкой премиальных строительных и расходных материалов'
            }
        ]
    }

    constructor(props) {
        super(props)
    }

    tabsHandler = (tabsName, event) => {
        const currentTab = event.currentTarget
        const tabs = [...this.state[tabsName]]
        tabs.forEach(tab => {
            if (+currentTab.dataset.id === tab.id) {
                tab.active = true
            } else {
                tab.active = false
            }
        })
        this.setState({tabsName: tabs})
    }

    serviceTableInputHandler = (tableId, event, tableKey) => {
        const tables = [...this.state.tables]
        tables.forEach(table => {
            if (table.id === tableId) {
                table[tableKey] = event.target.value
            }
        })
        this.setState({tables})
    }

    serviceTableBtnHandler = (tableId, event, tableKey) => {
        const tables = [...this.state.tables]
        tables.forEach(table => {
            if (table.id === tableId) {
                table[tableKey] = +event.currentTarget.dataset.id
            }
        })
        this.setState({tables})
    }

    serviceAddBtnHandler = () => {
        const tables = [...this.state.tables]
        const lastElement = tables[tables.length - 1]
        const lastElemenId = lastElement.id
        const tablePrototype = {
            'id': +lastElemenId + 1,
            'serviceId': 1,
            'amount': '',
            'dateStart': '',
            'days': '',
            'workAdditional': 2,
            'workType': 1
        }
        tables.push(tablePrototype)
        this.setState({tables})
    }

    serviceCalculateBtnHandler = () => {
        // доделать
        this.setState(prevState => {
            return {
                isServiceSelectPage: !prevState.isServiceSelectPage
            }
        })
    }
    deleteTableHandler = evt => {

        const id = evt.currentTarget.parentElement.dataset.id
        let removeIdx = null

        const tables = [...this.state.tables]
        tables.forEach((table, idx) => {
            if (table.id === + id) {
                removeIdx = idx
            }
        })

        if (removeIdx) {
            tables.splice(removeIdx, 1)
            this.setState({tables})
        }
    }

    render() {

        return (
            <React.Fragment>
                <div className="h1">{this.state.heading}</div>
                <ServicesTabs tabs={this.state.tabs} tabsHandler={this.tabsHandler}/>
                {this.state.tables.map((table, idx) => {
                    return (
                        <ServicesTable
                            key={idx}
                            rowNum = {idx+1}
                            table={table}
                            serviceTableInputHandler={this.serviceTableInputHandler}
                            serviceTableBtnHandler={this.serviceTableBtnHandler}
                            services={this.state.services}
                            workAdditional={this.state.workAdditional}
                            workType={this.state.workType}
                            deleteTableHandler={this.deleteTableHandler}
                        />
                    )
                })}
                <ServiceControls
                    serviceAddBtnHandler={this.serviceAddBtnHandler}
                    serviceCalculateBtnHandler={this.serviceCalculateBtnHandler}
                />
            </React.Fragment>
        )
    }
}

export default ServiceSelect