import React, {Component} from 'react'
import classes from './Services.module.scss'
import {Route, Switch, Redirect} from 'react-router-dom'
import ServiceSelect from '../ServiceSelect/ServiceSelect'
import ServiceCalc from '../ServiceCalc/ServiceCalc'


class Services extends Component {
    // constructor(props) {
    //     super(props)
    // }
    render() {
        const cls = [classes.Services]
        cls.push("container mt-2 pt-2 my-sm-5 p-sm-4")

        return (
            <div className={cls.join(' ')}>
                <Switch>
                    <Route path="/services/service_select" exact component={ServiceSelect}/>
                    <Route path="/services/calculation" component={ServiceCalc}/>
                    <Redirect to="/services/service_select"/>
                </Switch>
            </div>
        )
    }
}

export default Services