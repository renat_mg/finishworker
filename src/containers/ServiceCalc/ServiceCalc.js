import React from 'react'
import classes from './ServiceCalc.module.scss'
import {NavLink} from 'react-router-dom'
import ServiceCalcHeading from '../../components/ServiceCalcHeading/ServiceCalcHeading'
import ServiceCalcTabs from '../../components/ServiceCalcTabs/ServiceCalcTabs'
import ServiceCalcTable from '../../components/ServiceCalcTable/ServiceCalcTable'
import ServiceCalcBottom from '../../components/ServiceCalcBottom/ServiceCalcBottom'

import MaterialCalcTable from '../../components/MaterialCalcTable/MaterialCalcTable'


// import ServiceCalcTableWorks from '../../components/ServiceCalcTableWorks/ServiceCalcTableWorks'

class ServiceCalc extends React.Component {
    state = {
        currentTab: 1,
        order: {
            id: '123/2',
            date: '01.01.2020'
        },
        tabs: [
            {
                'id': 1,
                'name': 'Работы',
                'active': true
            },
            {
                'id': 2,
                'name': 'Материалы',
                'active': false
            }
        ],
        servicesTableHead: [
            {
                id: 1,
                name: '№'

            },
            {
                id: 2,
                name: 'Тип работы',
                col: 3
            },
            {
                id: 3,
                name: 'Ед. изм.'
            },
            {
                id: 4,
                name: 'Количество'
            },
            {
                id: 5,
                name: 'Цена, &#8381;',
                tooltip: 'Цена зависит от курса евро на дату заказа'
            },
            {
                id: 6,
                name: 'Сумма, &#8381;'

            },
            {
                id: 7,
                name: 'Начало работ'
            },
            {
                id: 8,
                name: 'Окончание работ'
            },
            {
                id: 9,
                name: 'Количество мастеров'
            },
            {
                id: 10,
                name: 'Удалить'
            },
        ],
        materialTableHead: [
            {
                id: 1,
                name: '№'

            },
            {
                id: 2,
                name: 'Материал',
                col: 3
            },
            {
                id: 3,
                name: 'Ед. изм.'
            },
            {
                id: 4,
                name: 'Количество'
            },
            {
                id: 5,
                name: 'Цена, &#8381;'
            },
            {
                id: 6,
                name: 'Сумма, &#8381;'

            },
            {
                id: 7,
                name: '',
                col: 3
            },
            {
                id: 8,
                name: 'Удалить'
            },
        ],
        tables: [
            {},
            {
                id: 1,
                serviceId: 1,
                amount: '700',
                dateStart: '2020-01-01',
                days: 5,
                workAdditional: 2,
                workType: 1,
                workerCount: '',
                cart: []
            },
            {
                id: 2,
                serviceId: 4,
                amount: '500',
                dateStart: '2019-12-29',
                days: 10,
                workAdditional: 1,
                workType: 1,
                workerCount: '',
                cart: []
            },
            {
                id: 3,
                serviceId: 3,
                amount: '450',
                dateStart: '2020-01-24',
                days: 5,
                workAdditional: 1,
                workType: 2,
                workerCount: '',
                cart: []
            },

        ],
        services: [
            {
                id: 1,
                name: 'Плиточные работы / полы',
                unit: 'м&sup2;',
                price: 1100,
                norm: 20,
                materials: [1]
            },
            {
                id: 2,
                name: 'Штукатурные работы',
                unit: 'м&sup2;',
                price: 300,
                norm: 36,
                materials: [2, 10]
            },
            {
                id: 3,
                name: 'Покрасочные работы',
                unit: 'м&sup2;',
                price: 50,
                norm: 50,
                materials: [3, 4, 5, 10]
            },
            {
                id: 4,
                name: 'Укладка ламината',
                unit: 'м&sup2;',
                price: 550,
                norm: 24,
                materials: [6, 7]
            },
            {
                id: 5,
                name: 'Поклейка обоев',
                unit: 'м&sup2;',
                price: 120,
                norm: 5,
                materials: [8, 9]
            }
        ],
        workAdditional: [
            {
                id: 1,
                name: 'Работа',
                tooltip: ''
            },
            {
                id: 2,
                name: 'Работа c поставкой материалов',
                tooltip: 'Расчет вместе с поставкой строительных и расходных материалов'
            }
        ],
        workType: [
            {
                id: 1,
                name: 'Стандарт',
                tooltip: ''
            },
            {
                id: 2,
                name: 'Премиум',
                tooltip: 'Расчет вместе с поставкой премиальных строительных и расходных материалов'
            }
        ],
        materials: [
            {
                id: 1,
                name: 'Плиточный клей',
                unit: 'кул',
                price: 200,
                norm: 0.3,
                weight: 25
            },
            {
                id: 2,
                name: 'Штукатурная смесь',
                unit: 'кул',
                price: 220,
                norm: 0.5,
                weight: 30
            },
            {
                id: 3,
                name: 'Краска',
                unit: 'л',
                price: 250,
                norm: 0.25,
                weight: 1
            },
            {
                id: 4,
                name: 'Расстворитель',
                unit: 'л',
                price: 180,
                norm: 0.1,
                weight: 1
            },
            {
                id: 5,
                name: 'Кисть',
                unit: 'шт',
                price: 100,
                norm: 0.02,
                weight: 0.2
            },
            {
                id: 6,
                name: 'Ламинат',
                unit: 'м&sup2;',
                price: 500,
                norm: 1,
                weight: 2
            },
            {
                id: 7,
                name: 'Подложка',
                unit: 'м&sup2;',
                price: 50,
                norm: 1,
                weight: .5
            },
            {
                id: 8,
                name: 'Обои',
                unit: 'рул',
                price: 2000,
                norm: 0.15,
                weight: 1
            },
            {
                id: 9,
                name: 'Клей',
                unit: 'уп',
                price: 500,
                norm: 0.05,
                weight: .4
            },
            {
                id: 10,
                name: 'Грунтовка',
                unit: 'л',
                price: 50,
                norm: 0.5,
                weight: 1
            },

        ]
    }

    tabsHandler = (tabsName, event) => {
        const currentTab = +event.currentTarget.dataset.id
        const tabs = [...this.state[tabsName]]
        tabs.forEach(tab => {
            if (currentTab === tab.id) {
                tab.active = true
            } else {
                tab.active = false
            }
        })
        console.log(currentTab)
        if (currentTab === 2) {
            const tables = [...this.state.tables]
            tables.forEach(table => {
                if (table.id) {

                    let service = this.getService(table.id)
                    let materials = this.getMaterial(service.materials)
                    table.materials = materials.map(material => {
                        return {
                            id: material.id,
                            name: material.name,
                            price: material.price,
                            unit: material.unit,
                            amount: Math.ceil(table.amount * material.norm)
                        }
                    })
                    if (table.cart.length === 0) {
                        table.cart = materials.map(material => {
                            return {
                                id: material.id,
                                changed: false,
                                need: true,
                                amount: null
                            }
                        })
                    }
                }
            })

            this.setState({tables})
        }


        this.setState({tabs, currentTab})
    }

    tableHandler = (tableId, count, tableKey) => {
        const tables = [...this.state.tables]
        tables.forEach(table => {
            if (table.id === tableId) {
                table[tableKey] = count
            }
        })

        this.setState({tables})
    }
    deleteTableHandler = evt => {
        //   console.log()
        const id = evt.currentTarget.parentElement.dataset.id
        let removeIdx = null

        const tables = [...this.state.tables]
        tables.forEach((table, idx) => {
            if (table.id === +id) {
                removeIdx = idx
            }
        })

        if (removeIdx) {
            tables.splice(removeIdx, 1)
            this.setState({tables})
        }
    }
    workerCounHandler = () => {
        const tables = [...this.state.tables]
        tables.forEach(table => {
            let service = this.getService(table.id)
            if (table.id) {
                let workerCount = table.amount / (service.norm * table.days)
                if (workerCount < 1) {
                    workerCount = 1
                }
                workerCount = Math.round(workerCount)
                table['workerCount'] = workerCount
            }
        })

        this.setState({tables})
    }
    getWorkerCount = tableId => {
        const tables = [...this.state.tables]
        let workerCount = null
        tables.forEach(table => {
            if (+tableId === +table.id) {
                let service = this.getService(tableId)

                if (service) {
                    if (!table.days) {
                        table.days = 1
                    }
                    workerCount = table.amount / (service.norm * table.days)
                    if (workerCount < 1) {
                        workerCount = 1
                    }

                }
            }
        })
        return Math.round(workerCount)
    }
    getService = tableId => {
        let service = this.state.services.filter(service => {
            return +tableId === service.id
        })
        return service[0]
    }
    getMaterial = serviceMaterials => {
        // временное решение, потом сделать запросом к бд, хотя...

        let mareials = this.state.materials.filter(material => {
            return serviceMaterials.indexOf(material.id) !== -1
        })
        return mareials
    }
    getSum = tableId => {
        const tables = [...this.state.tables]
        let sum = null
        tables.forEach(table => {
            if (+tableId === +table.id) {
                let service = this.getService(tableId)
                sum = service.price * table.amount
            }
        })
        return sum
    }
    deleteMaterialHandler = evt => {
        const tableId = evt.currentTarget.parentElement.dataset.table
        const materialId = evt.currentTarget.parentElement.dataset.material

       const tables = [...this.state.tables]
        tables.forEach(table => {
            if (table.id === +tableId) {
                console.log(table)
                const cart = table.cart
                cart.forEach(material => {
                   if(material.id === +materialId){
                       material.need = false
                   }
                })
            }
        })

       this.setState({tables})
    }


    render() {
        console.log(this.state)

        return (
            <React.Fragment>
                <h1>
                    <NavLink to="/services/service_select">Service Calc</NavLink>
                </h1>

                <ServiceCalcHeading
                    order={this.state.order}
                />
                <div className={classes.ServiceCalcContainer}>
                    <ServiceCalcTabs
                        tabs={this.state.tabs}
                        tabsHandler={this.tabsHandler}
                    />
                    {this.state.currentTab === 1
                        ?
                        <ServiceCalcTable
                            tableHead={this.state.servicesTableHead}
                            tables={this.state.tables}
                            services={this.state.services}
                            workAdditional={this.state.workAdditional}
                            workType={this.state.workType}
                            tableHandler={this.tableHandler}
                            deleteTableHandler={this.deleteTableHandler}
                            getService={this.getService}
                            getWorkerCount={this.getWorkerCount}
                            getSum={this.getSum}
                        />
                        : <MaterialCalcTable
                            tableHead={this.state.materialTableHead}
                            tables={this.state.tables}
                            services={this.state.services}
                            materials={this.state.materials}

                            getService={this.getService}
                            getMaterial={this.getMaterial}
                            deleteMaterialHandler = {this.deleteMaterialHandler}
                        />
                    }

                </div>
                {this.state.currentTab === 1
                    ?
                    <ServiceCalcBottom
                        tables={this.state.tables}
                        getService={this.getService}
                        getWorkerCount={this.getWorkerCount}
                        getSum={this.getSum}

                    />
                    : null
                }


            </React.Fragment>
        )
    }
}

export default ServiceCalc