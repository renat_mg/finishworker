import React, {Component} from 'react'
import clasess from './Layout.module.scss'

class Layout extends Component {
    render() {
        const cls = [
            clasess.Main,
            "d-flex flex-column justify-content-between"
        ]
        return (
            <main className={cls.join(' ')}>
                {this.props.children}
            </main>
        )
    }
}

export default Layout